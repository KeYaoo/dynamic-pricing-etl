
import glob
import numpy as np
import pandas as pd
import argparse
import json
import logging
import os
import sys
import torch
import torch.distributed as dist
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data
import torch.utils.data.distributed

from torch.optim.lr_scheduler import StepLR
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler(sys.stdout))

class PlusgradeDataset(Dataset):
    def __init__(self, data_path, transform=None):
        self.data, self.labels = self.get_data_labels(data_path)
        self.transform = transform
    
    def __len__(self):
        return len(self.labels)

    def __getitem__(self, index):
        return self.data[index], self.labels[index]

    def get_data_labels(self, data_path):
        all_files = glob.glob(os.path.join(data_path, "part*"))
        sharded_dataset = (pd.read_csv(f, header=None, engine='python') for f in all_files)
        dataset = pd.concat(sharded_dataset, ignore_index=True)

        dataset = dataset.to_numpy().astype(np.float32)

        labels = dataset[:,0]
        data = dataset[:,2:]

        #scaler
        m = data.mean(axis=0)
        s = data.std(axis=0)
        data -= m
        data /= s

        data = np.nan_to_num(data)
        
        return self.create_input_sequence(data, labels)

    def transform_data(self, input_data, input_labels, time_steps=2):

        input_size = input_data.shape[1]

        x, y = [], []
        for i in range(len(input_data) - time_steps):
            x_i = input_data[i:i + time_steps]
            y_i = input_data[i+1:i+time_steps+1]
            x.append(x_i)
            y.append(y_i)
        x_arr = np.array(x).reshape(-1, time_steps)
        y_arr = np.array(y).reshape(-1, time_steps)

        x_var = Variable(torch.from_numpy(x_arr).float())
        y_var = Variable(torch.from_numpy(y_arr).float())
        return x_var, y_var

    def create_input_sequence(self, input_data, input_labels, time_steps=2):
        train_sequence = []
        train_labels = []
        L = len(input_data)

        for i in range(L - time_steps):
            train_seq = input_data[i:i+time_steps]
            train_label = input_labels[i+time_steps:i+time_steps+1]
            train_sequence.append(train_seq)
            train_labels.append(train_label)

        #train_sequence = torch.from_numpy(np.array(train_sequence))
        #train_sequence = train_sequence.transpose(1,0)
        
        return train_sequence, train_labels
    

def _get_train_data_loader(batch_size, training_dir, is_distributed, **kwargs):
    logger.info("Get train data sampler and data loader")
    dataset = PlusgradeDataset(training_dir)
    train_sampler = torch.utils.data.distributed.DistributedSampler(dataset) if is_distributed else None
    train_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=train_sampler is None, sampler=train_sampler, **kwargs)
    return train_loader

def _get_validation_data_loader(batch_size, validation_dir, **kwargs):
    logger.info("Get validation data sampler and data loader")
    dataset = PlusgradeDataset(validation_dir)
    #validation_sampler = torch.utils.data.distributed.DistributedSampler(dataset, num_replicas=hvd.size(), rank=hvd.rank())
    validation_loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, sampler=None, **kwargs)
    return validation_loader

def test(model, test_loader, device):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for data, target in test_loader:
            data = data.transpose(1,0)
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.mse_loss(output, target, size_average=False).item()  # sum up batch loss
    test_loss /= len(test_loader.dataset)
    logger.info('Test set: Average loss: {:.4f}'.format(
        test_loss))


class TransformerNet(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(TransformerNet, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.lstm = nn.LSTMCell(self.input_size, self.hidden_size)
        self.fc = nn.Sequential(
            nn.Linear(self.hidden_size, self.hidden_size),
            nn.ReLU(),
            nn.Linear(self.hidden_size, self.output_size)
        )

    def forward(self, input):

        #print('input', input.size())
        outputs = []

        # reset the state of LSTM
        # the state is kept till the end of the sequence
        h_t = torch.zeros(input.size(1), self.hidden_size, dtype=torch.float32)
        c_t = torch.zeros(input.size(1), self.hidden_size, dtype=torch.float32)
        #print('h_t', h_t.size())
        #print(c_t.size())

        for i in range(input.size()[0]):
            h_t, c_t = self.lstm(input[i], (h_t, c_t))
            output = self.fc(h_t)
            outputs.append(output)

        # for i, input_t in enumerate(input.chunk(input.size(1), dim=1)):
        #     print('input_t', input_t.size())
        #     h_t, c_t = self.lstm(input_t, (h_t, c_t))
        #     output = self.fc(h_t)
        #     outputs += [output]
        #outputs = torch.stack(outputs, 1).squeeze(2)
        outputs = torch.stack(outputs, dim=0)
        outputs = torch.mean(outputs, dim=0)
        print('outputs', outputs.size())

        return outputs

# class TransformerNet(nn.Module):
#     def __init__(self, input_size, hidden_size, output_size):
#         super(TransformerNet, self).__init__()
#         self.input_size = input_size
#         self.hidden_size = hidden_size
#         self.output_size = output_size
#         self.lstm = nn.LSTM(self.input_size, self.hidden_size)
#         self.fc = nn.Sequential(
#             nn.Linear(self.hidden_size, self.hidden_size),
#             nn.ReLU(),
#             nn.Linear(self.hidden_size, self.output_size)
#         )
#         self.linear = nn.Linear(self.hidden_size, self.output_size)

#     def forward(self, x):

#         y = self.lstm(x)[0]
#         time_steps, batch_size, input_size = y.shape
#         y = y.view(-1, input_size)
#         y = self.fc(y)
#         y = y.view(time_steps, batch_size, -1)

#         print('y', y.size())
#         return y


def train(args):
    is_distributed = False
    logger.debug("Distributed training - {}".format(is_distributed))
    use_cuda = args.num_gpus > 0
    logger.debug("Number of gpus available - {}".format(args.num_gpus))
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    device = torch.device("cuda" if use_cuda else "cpu")

    if is_distributed:
        # Initialize the distributed environment.
        world_size = len(args.hosts)
        os.environ['WORLD_SIZE'] = str(world_size)
        host_rank = args.hosts.index(args.current_host)
        os.environ['RANK'] = str(host_rank)
        dist.init_process_group(backend=args.backend, rank=host_rank, world_size=world_size)
        logger.info('Initialized the distributed environment: \'{}\' backend on {} nodes. '.format(
            args.backend, dist.get_world_size()) + 'Current host rank is {}. Number of gpus: {}'.format(
            dist.get_rank(), args.num_gpus))

    # set the seed for generating random numbers
    torch.manual_seed(args.seed)
    if use_cuda:
        torch.cuda.manual_seed(args.seed)

    train_loader = _get_train_data_loader(args.batch_size, args.data_dir, is_distributed, **kwargs)
    validation_loader = _get_validation_data_loader(args.test_batch_size, './validation', **kwargs)

    logger.debug("Processes {}/{} ({:.0f}%) of train data".format(
        len(train_loader.sampler), len(train_loader.dataset),
        100. * len(train_loader.sampler) / len(train_loader.dataset)
    ))

    logger.debug("Processes {}/{} ({:.0f}%) of test data".format(
        len(validation_loader.sampler), len(validation_loader.dataset),
        100. * len(validation_loader.sampler) / len(validation_loader.dataset)
    ))

    model = TransformerNet(input_size=36, hidden_size=21, output_size=1).to(device)
    if is_distributed and use_cuda:
        # multi-machine multi-gpu case
        model = torch.nn.parallel.DistributedDataParallel(model)
    else:
        # single-machine multi-gpu case or single-machine or multi-machine cpu case
        model = torch.nn.DataParallel(model)

    loss = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=args.lr)
    scheduler = StepLR(optimizer, step_size=args.step_size, gamma=0.1)

    for epoch in range(1, args.epochs + 1):
        model.train()
        for batch_idx, (data, target) in enumerate(train_loader, 1):
            data = data.transpose(1,0)
            #print('data', data.size())
            data, target = data.to(device), target.to(device)
            optimizer.zero_grad()
            output = model(data)
            #print('output', output.size())
            batch_loss = loss(output, target)
            batch_loss.backward()
            if is_distributed and not use_cuda:
                # average gradients manually for multi-machine cpu case only
                _average_gradients(model)
            optimizer.step()
            if batch_idx % args.log_interval == 0:
                logger.info('Train Epoch: {} [{}/{} ({:.0f}%)] Batch Loss: {:.6f}'.format(
                    epoch, batch_idx * len(data), len(train_loader.sampler),
                    100. * batch_idx / len(train_loader), batch_loss.item()))
        test(model, validation_loader, device)
        scheduler.step()
    save_model(model, args.model_dir)
    

def model_fn(model_dir):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = torch.nn.DataParallel(Net())
    with open(os.path.join(model_dir, 'model.pth'), 'rb') as f:
        model.load_state_dict(torch.load(f))
    return model.to(device)

def save_model(model, model_dir):
    logger.info("Saving the model.")
    path = os.path.join(model_dir, 'model.pth')
    # recommended way from http://pytorch.org/docs/master/notes/serialization.html
    torch.save(model.cpu().state_dict(), path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Data and model checkpoints directories
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=64, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=30, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.001)')
    parser.add_argument('--step-size', type=int, default=10, metavar='N',
                        help='learning rate decay (default: 10)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=100, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--backend', type=str, default=None,
                        help='backend for distributed training (tcp, gloo on cpu and gloo, nccl on gpu)')

    # Container environment
    parser.add_argument('--hosts', type=list, default=[])
    parser.add_argument('--current-host', type=str, default='')
    parser.add_argument('--model-dir', type=str, default='./')
    parser.add_argument('--data-dir', type=str, default='./')
    parser.add_argument('--num-gpus', type=int, default=0)

    train(parser.parse_args())
